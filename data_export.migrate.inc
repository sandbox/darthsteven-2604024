<?php

/**
 * Implements hook_migrate_api().
 */
function data_export_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'data_export' => array(
        'title' => t('Data Export'),
      ),
    ),

    'migrations' => array(
      'DataExportTest' => array(
        'class_name' => 'DataExportTestMigration',
        'group_name' => 'data_export',
      ),
    ),
  );
  return $api;
}