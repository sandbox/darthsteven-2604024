<?php

class DataExportTestMigration extends Migration {
  public function __construct($arguments = array()) {
    parent::__construct($arguments);

    $query = db_select('node', 'n')
      ->fields('n', array('nid', 'vid', 'title', 'uid', 'language'))
      // This sort assures that parents are saved before children.
      ->orderBy('nid', 'ASC');

    // Create a MigrateSource object, which manages retrieving the input data.
    $source_options = array(
      'map_joinable' => FALSE,
    );
    $this->source = new MigrateSourceSQL($query, array(), NULL, $source_options);

    $file_uri = 'public://data-export.csv';
    $this->destination = new MigrateDestinationCSV($file_uri);
    $this->destination->setFields(array(
      'nid' => 'NodeID',
      'vid' => 'Version string',
      'title' => 'Title',
      'uid' => 'User',
      'language' => 'Languages',
    ));

    $this->addSimpleMappings(array(
      'nid',
      'vid',
      'title',
      'uid',
      'language',
    ));

    $this->map = new DataExportMigrateMap();
  }
}
