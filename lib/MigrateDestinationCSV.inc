<?php

class MigrateDestinationCSV extends MigrateDestination {

  static public function getKeySchema() {
    return array(
//      'row' => array(
//        'type' => 'int',
//        'unsigned' => TRUE,
//        'description' => 'ID of destination',
//      ),
    );
  }

  public function __toString() {
    return t('CSV file');
  }

  protected $fields = array();
  public function fields() {
    return $this->fields;
  }

  public function setFields(array $fields) {
    $this->fields = $fields;
    return $this;
  }

  protected $file_uri;
  protected $handle;

  public function __construct($file_uri, $options = array()) {
    $this->file_uri = $file_uri;
    parent::__construct();
  }


  protected function ensureFileHandle() {
    if (!isset($this->handle)) {
      $this->handle = fopen($this->file_uri, 'a');
    }
    return $this->handle;
  }

  /**
   * @inheritDoc
   */
  function __destruct() {
    if ($handler = $this->ensureFileHandle()) {
      fclose($handler);
    }
  }

  public function import(stdClass $exportrow, stdClass $sourcerow) {
    // Invoke migration prepare handlers
    $this->prepare($exportrow, $sourcerow);
    // Let's just send the row into the file.
    $s = $this->ensureFileHandle();
    fputcsv($s, (array) $sourcerow);

    $this->complete($exportrow, $sourcerow);

    return array(1);
  }

  /**
   * Implementation of MigrateDestination::prepare().
   */
  public function prepare($exportrow, stdClass $row) {
    // We do nothing here but allow child classes to act.
    $migration = Migration::currentMigration();
    $exportrow->migrate = array(
      'machineName' => $migration->getMachineName(),
    );

    // Call any general handlers.
    migrate_handler_invoke_all('data_export', 'prepare', $exportrow, $row);
    // Then call any prepare handler for this specific Migration.
    if (method_exists($migration, 'prepare')) {
      $migration->prepare($exportrow, $row);
    }
  }

  public function complete($exportrow, stdClass $row) {
    // We do nothing here but allow child classes to act.
    $migration = Migration::currentMigration();
    $exportrow->migrate = array(
      'machineName' => $migration->getMachineName(),
    );
    // Call any general handlers.
    migrate_handler_invoke_all('data_export', 'complete', $exportrow, $row);
    // Then call any complete handler for this specific Migration.
    if (method_exists($migration, 'complete')) {
      $migration->complete($exportrow, $row);
    }
  }
}
