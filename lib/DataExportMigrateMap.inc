<?php

class DataExportMigrateMap extends MigrateMap {
  public function getSourceKey() {
    return array();
  }

  public function getDestinationKey() {
    return array();
  }

  /**
   * @inheritDoc
   */
  public function saveIDMapping(stdClass $source_row, array $dest_ids,
                                $status = MigrateMap::STATUS_IMPORTED,
                                $rollback_action = MigrateMap::ROLLBACK_DELETE, $hash = NULL) {
    // TODO: Implement saveIDMapping() method.
  }

  /**
   * @inheritDoc
   */
  public function saveMessage($source_key, $message, $level = MigrationBase::MESSAGE_ERROR) {
    // TODO: Implement saveMessage() method.
  }

  /**
   * @inheritDoc
   */
  public function prepareUpdate() {
    // TODO: Implement prepareUpdate() method.
  }

  /**
   * @inheritDoc
   */
  public function processedCount() {
    // TODO: Implement processedCount() method.
  }

  /**
   * @inheritDoc
   */
  public function importedCount() {
    // TODO: Implement importedCount() method.
  }

  /**
   * @inheritDoc
   */
  public function errorCount() {
    // TODO: Implement errorCount() method.
  }

  /**
   * @inheritDoc
   */
  public function messageCount() {
    // TODO: Implement messageCount() method.
  }

  /**
   * @inheritDoc
   */
  public function delete(array $source_key, $messages_only = FALSE) {
    // TODO: Implement delete() method.
  }

  /**
   * @inheritDoc
   */
  public function deleteDestination(array $destination_key) {
    // TODO: Implement deleteDestination() method.
  }

  /**
   * @inheritDoc
   */
  public function deleteBulk(array $source_keys) {
    // TODO: Implement deleteBulk() method.
  }

  /**
   * @inheritDoc
   */
  public function clearMessages() {
    // TODO: Implement clearMessages() method.
  }

  /**
   * @inheritDoc
   */
  public function getRowBySource(array $source_id) {
    // TODO: Implement getRowBySource() method.
  }

  public function getRowByDestination(array $destination_id) {
    // TODO: Implement getRowByDestination() method.
  }

  /**
   * @inheritDoc
   */
  public function getRowsNeedingUpdate($count) {
    // TODO: Implement getRowsNeedingUpdate() method.
  }

  /**
   * @inheritDoc
   */
  public function lookupSourceID(array $destination_id) {
    // TODO: Implement lookupSourceID() method.
  }

  /**
   * @inheritDoc
   */
  public function lookupDestinationID(array $source_id) {
    // TODO: Implement lookupDestinationID() method.
  }

  /**
   * @inheritDoc
   */
  public function destroy() {
    // TODO: Implement destroy() method.
  }

  /**
   * @inheritDoc
   */
  public function current() {
    // TODO: Implement current() method.
  }

  /**
   * @inheritDoc
   */
  public function next() {
    // TODO: Implement next() method.
  }

  /**
   * @inheritDoc
   */
  public function key() {
    // TODO: Implement key() method.
  }

  /**
   * @inheritDoc
   */
  public function valid() {
    // TODO: Implement valid() method.
  }

  /**
   * @inheritDoc
   */
  public function rewind() {
    // TODO: Implement rewind() method.
  }


}